part of 'user_bloc.dart';

abstract class UserState extends Equatable {
  const UserState();

  @override
  List<Object> get props => [];
}

class UserInitial extends UserState {}

class UserLoading extends UserState {}

class UserLoaded extends UserState {
  final List<Results>? users;

  UserLoaded({required this.users});
  @override
  List<Object> get props => [users!];
}

class UserError extends UserState {
  final String? errorMessage;

  const UserError({this.errorMessage});

  @override
  List<Object> get props => [errorMessage!];
}


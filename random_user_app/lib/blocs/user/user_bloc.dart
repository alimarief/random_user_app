import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:random_user_app/services/user_service.dart';

import '../../models/user_model.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final UserService _userService = UserService();
  List<Results>? users = [];
  List<Results>? searchResult = [];

  UserBloc() : super(UserInitial()) {
    on<FetchUsers>((event, emit) async {
      emit(UserLoading());
      UserModel apiResult = await _userService.getUsers();

      users = apiResult.results;

      emit(UserLoaded(users: users!));
    });

  }
}

import 'package:dio/dio.dart';

import '../models/user_model.dart';


class UserService {
  late Response response;
  late Dio _dio = Dio();

  Future<UserModel> getUsers() async {
    response = await _dio.get("https://randomuser.me/api/?results=30");

    try {
      if (response.statusCode == 200) {
        return UserModel.fromJson(response.data);
      }
      return UserModel.fromJson(response.data);
    } on DioError catch (e) {
      print(e);
      return UserModel(
        results: [],
      );
    }
  }
}

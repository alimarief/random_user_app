import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:random_user_app/const.dart';
import 'package:random_user_app/models/user_model.dart';
import 'package:random_user_app/ui/pages/pages.dart';

class FriendsItem extends StatelessWidget {
  final Results user;
  const FriendsItem({Key? key, required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Get.to(DetailPage(user: user));
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 10),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  width: 50,
                  height: 50,
                  margin: const EdgeInsets.only(
                    right: 10,
                    left: 10,
                  ),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: NetworkImage(user.picture!.medium!),
                    ),
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${user.name!.first!} ${user.name!.last!}",
                        style: primaryTextStyle.copyWith(
                          fontWeight: medium,
                        ),
                      ),
                      Text(
                        "${user.phone}",
                        style: secondaryTextStyle.copyWith(
                          fontWeight: medium,
                        ),
                      )
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Image.asset(
                      'assets/icon_twitter.png',
                      width: 24,
                    ),
                    Text(
                      "@${user.name!.first}",
                      style: secondaryTextStyle.copyWith(
                        fontWeight: medium,
                      ),
                    )
                  ],
                ),
              ],
            ),
            const SizedBox(height: 5),
            Divider(
              color: subtitleColor,
              thickness: 0.5,
            )
          ],
        ),
      ),
    );
  }
}

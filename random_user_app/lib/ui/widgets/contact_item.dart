import 'package:flutter/material.dart';

import '../../const.dart';

class ContactItem extends StatelessWidget {

  final String title;
  final String subtitle;
  final Icon icon;
  final Icon iconLeading;

  const ContactItem({
    Key? key,
    required this.icon,
    required this.iconLeading,
    required this.title,
    required this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            iconLeading,
            const SizedBox(width: 20),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: primaryTextStyle.copyWith(
                      fontWeight: medium,
                    ),
                  ),
                  Text(
                    subtitle,
                    style: secondaryTextStyle.copyWith(
                      fontWeight: medium,
                    ),
                  )
                ],
              ),
            ),
            Container(
              width: 25,
              height: 25,
              decoration: BoxDecoration(
                color: Colors.greenAccent[400],
                shape: BoxShape.circle,
              ),
              child: icon,
            ),
            const SizedBox(width: 10),
            // Container(
            //   width: 25,
            //   height: 25,
            //   decoration: BoxDecoration(
            //     color: Colors.greenAccent[400],
            //     shape: BoxShape.circle,
            //   ),
            //   child: const Icon(
            //     Icons.chat,
            //     size: 15,
            //   ),
            // ),
          ],
        ),
        const SizedBox(height: 5),
        Divider(
          color: subtitleColor,
          thickness: 0.5,
        )
      ],
    );
  }
}

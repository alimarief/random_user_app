import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../const.dart';
import '../../models/user_model.dart';
import '../pages/pages.dart';

class RecentItem extends StatelessWidget {
  final Results user;

  const RecentItem({
    Key? key,
    required this.user,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.to(DetailPage(user: user));
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Stack(
            children: [
              Container(
                width: 70,
                height: 70,
                margin: const EdgeInsets.only(
                  right: 10,
                  left: 10,
                ),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: NetworkImage(user.picture!.medium!),
                  ),
                ),
              ),
              Positioned(
                right: 5,
                child: Container(
                  height: 20,
                  width: 20,
                  decoration: BoxDecoration(
                    border: Border.all(),
                    shape: BoxShape.circle,
                    color: Colors.green[400],
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 5),
          Text(
            textAlign: TextAlign.center,
            user.name!.first!,
            style: primaryTextStyle,
          ),
        ],
      ),
    );
  }
}

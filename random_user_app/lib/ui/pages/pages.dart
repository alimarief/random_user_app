
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:random_user_app/models/user_model.dart';
import 'package:random_user_app/ui/widgets/contact_item.dart';
import 'package:random_user_app/ui/widgets/friends_item.dart';

import '../../blocs/user/user_bloc.dart';
import '../../const.dart';
import '../../ui/widgets/recent_item.dart';

part 'home_page.dart';
part 'detail_page.dart';
part of 'pages.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController searchInput = TextEditingController();
  List<Results> usersOnSearch = [];

  @override
  Widget build(BuildContext context) {
    int? totalFriends = context.watch<UserBloc>().users?.length;
    List<Results>? usersList = context.watch<UserBloc>().users!;

    return Scaffold(
      backgroundColor: backgroundColor2,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  left: 24,
                  top: 10,
                ),
                child: Text(
                  'My Contact',
                  style: primaryTextStyle.copyWith(
                    fontSize: 24,
                    fontWeight: bold,
                  ),
                ),
              ),
              // SEARCH BOX
              Container(
                height: 56,
                margin: const EdgeInsets.fromLTRB(20, 32, 20, 24),
                decoration: BoxDecoration(
                    color: grayColor, borderRadius: BorderRadius.circular(20)),
                child: Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(left: 25),
                      child:
                          FaIcon(FontAwesomeIcons.search, color: Colors.white),
                    ),
                    Container(
                      width: 250,
                      margin: const EdgeInsets.only(left: 20),
                      child: TextField(
                        controller: searchInput,
                        onChanged: (value) {
                          setState(() {
                            usersOnSearch = usersList
                                .where((element) =>
                                    element.name!.first!.contains(value))
                                .toList();
                          });
                        },
                        style: primaryTextStyle,
                        textCapitalization: TextCapitalization.sentences,
                        decoration: InputDecoration(
                          hintStyle: GoogleFonts.poppins(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.w400),
                          hintText: "Search",
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          contentPadding: const EdgeInsets.only(
                              left: 0, bottom: 11, top: 11, right: 15),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 24,
                  top: 10,
                  bottom: 10,
                ),
                child: Text(
                  'Recent',
                  style: primaryTextStyle.copyWith(
                    fontSize: 18,
                    fontWeight: bold,
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              BlocBuilder<UserBloc, UserState>(
                builder: (context, state) {
                  if (state is UserLoading) {
                    return const Center(child: CircularProgressIndicator());
                  }
                  if (state is UserLoaded) {
                    return Container(
                      height: 120,
                      child: ListView.builder(
                        padding: const EdgeInsets.symmetric(horizontal: 14),
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: state.users!.length,
                        itemBuilder: ((context, index) {
                          return RecentItem(user: state.users![index]);
                        }),
                      ),
                    );
                  } else {
                    return Center(
                      child: Text(
                        'Something went wrong',
                        style: primaryTextStyle,
                      ),
                    );
                  }
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Friends(${totalFriends ?? 0})',
                      style: primaryTextStyle.copyWith(
                        fontSize: 18,
                        fontWeight: bold,
                      ),
                    ),
                    Text(
                      'Show All',
                      style: secondaryTextStyle.copyWith(
                        decoration: TextDecoration.underline,
                        fontSize: 14,
                        fontWeight: bold,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 20),
              BlocBuilder<UserBloc, UserState>(
                builder: (context, state) {
                  if (state is UserLoading) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  if (state is UserLoaded) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 24),
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: searchInput.text.isNotEmpty
                            ? usersOnSearch.length
                            : usersList.length,
                        itemBuilder: ((context, index) {
                          return FriendsItem(
                              user: searchInput.text.isNotEmpty
                                  ? usersOnSearch[index]
                                  : usersList[index]);
                        }),
                      ),
                    );
                  } else {
                    return Center(
                      child: Text(
                        'Something went wrong',
                        style: primaryTextStyle,
                      ),
                    );
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}

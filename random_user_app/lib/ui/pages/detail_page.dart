part of 'pages.dart';

class DetailPage extends StatelessWidget {
  final Results user;
  const DetailPage({Key? key, required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor2,
      appBar: AppBar(
        backgroundColor: backgroundColor2,
        elevation: 0,
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.menu_rounded),
          ),
        ],
      ),
      body: Container(
        width: double.infinity,
        margin: const EdgeInsets.symmetric(
          horizontal: 24,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 120,
              height: 120,
              margin: const EdgeInsets.only(
                bottom: 20,
              ),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: NetworkImage(user.picture!.medium!),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Text(
              fullname(user.name!.title!, user.name!.first!, user.name!.last!),
              style: primaryTextStyle.copyWith(
                fontSize: 18,
                fontWeight: semiBold,
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Text(
              address(user.location!.state!, user.location!.country!),
              style: primaryTextStyle.copyWith(
                fontSize: 14,
                fontWeight: medium,
              ),
            ),
            const SizedBox(height: 100),
            ContactItem(
              title: user.phone!,
              subtitle: 'number - ${user.location!.country}',
              iconLeading: Icon(
                Icons.phone,
                color: blueColor,
                size: 35,
              ),
              icon: const Icon(
                Icons.phone_sharp,
                size: 15,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            ContactItem(
              title: user.email!,
              subtitle: 'Email Account',
              iconLeading: Icon(
                Icons.mail_rounded,
                color: blueColor,
                size: 35,
              ),
              icon: const Icon(
                Icons.mail_rounded,
                size: 15,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
